import React, { useState } from 'react';

// function MeuElemento(props){
const MeuBotao = (props) => {
  const [clicked, setClicked] = React.useState(false);
  if (clicked)
    return ("Clicado com Hook");

  return (
    <button onClick={() => {
      setClicked(true);
    }} >
      Clique aqui!
    </button>
  )
}

export default MeuBotao;