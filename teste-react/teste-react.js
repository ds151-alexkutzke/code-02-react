'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var el = React.createElement;

// function MeuElemento(props){
var MeuElemento = function MeuElemento(props) {
  var _React$useState = React.useState(false),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      clicked = _React$useState2[0],
      setClicked = _React$useState2[1];

  if (clicked) return "Clicado com Hook";

  return React.createElement(
    "button",
    { onClick: function onClick() {
        setClicked(true);
      } },
    "Clique aqui!"
  );
};

var MeuBotao = function (_React$Component) {
  _inherits(MeuBotao, _React$Component);

  function MeuBotao(props) {
    _classCallCheck(this, MeuBotao);

    var _this = _possibleConstructorReturn(this, (MeuBotao.__proto__ || Object.getPrototypeOf(MeuBotao)).call(this, props));

    _this.state = { clicked: false };
    return _this;
  }

  _createClass(MeuBotao, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      if (this.state.clicked) return "Clicado!!";

      return React.createElement(
        "button",
        { onClick: function onClick() {
            _this2.setState({ clicked: true });
            console.log('clicou!');
          } },
        "Clique aqui!"
      );
    }
  }]);

  return MeuBotao;
}(React.Component);

var domContainer = document.querySelector('#react');
ReactDOM.render(React.createElement(
  "div",
  null,
  React.createElement(MeuElemento, null),
  React.createElement(MeuElemento, null),
  React.createElement(MeuBotao, null),
  React.createElement(MeuBotao, null)
), domContainer);