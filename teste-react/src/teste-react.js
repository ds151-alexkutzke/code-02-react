'use strict';

const el = React.createElement;

// function MeuElemento(props){
const MeuElemento = (props) => {
  const [clicked, setClicked] = React.useState(false);
  if (clicked)
    return("Clicado com Hook");

  return (
    <button onClick={() => {
      setClicked(true);
    }} >
      Clique aqui!
    </button>
  )
}

class MeuBotao extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clicked: false };
  }

  render() {
    if (this.state.clicked)
      return ("Clicado!!");

    return (
      <button onClick={() => {
        this.setState({ clicked: true });
        console.log('clicou!');
      }} >
        Clique aqui!
      </button>
    )
  }
}

const domContainer = document.querySelector('#react');
ReactDOM.render(
  <div>
    <MeuElemento />
    <MeuElemento />
    <MeuBotao />
    <MeuBotao />
  </div>
  , domContainer);